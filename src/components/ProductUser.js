import React, {useState, useEffect} from 'react';

import{Container} from 'react-bootstrap';

import Product from '../pages/Products';
import ProductCard from './ProductCard';

export default function ProductUser() {
	
	const [product, setProducts] = useState([])


	useEffect(() => {
		fetch('https://evening-ravine-83251.herokuapp.com/products')
		.then(res => res.json())
		.then(productData => {
		
		setProducts(productData.map(product => {
				return(
					<ProductCard key={product._id} productProp={product}/>
				);
			}));
		})
	}, [])


	return (
		<Container>
			{product}
		</Container>
	)
}
