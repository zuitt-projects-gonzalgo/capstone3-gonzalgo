import React, {useContext} from 'react';
import {Navbar, Container, Nav, Form, FormControl, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import UserContext from '../UserContext';
import Search from '../pages/Search';

import '../App.css';

export default function AppNavbar() {

	const {user} = useContext(UserContext);

	return(
		<Navbar bg="success" variant="dark" expand="lg" sticky="top">
		  <Container>
		    <Navbar.Brand className="brandName" as={Link} to="/">JDG</Navbar.Brand>
		  	<Form className="d-flex alignment m-auto">
	  	        <FormControl
	  	          type="search"
	  	          placeholder="Search"
	  	          className="me-2"
	  	          aria-label="Search"
	  	        />

	  	        <Button as={Link} to="/products" variant="outline-light"><i className="fa-solid fa-magnifying-glass"></i></Button>
		  	</Form>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav">
		      <Nav className="ms-auto navLink">
		      	<Nav.Link className="m-3" as={Link} to="/">Home</Nav.Link>
		      	<Nav.Link className="m-3" as={Link} to="/products">Products</Nav.Link>

		        {(user.id !== null) ?
		        	<>
		        	<Nav.Link className="m-3" as={Link} to="/logout">Logout</Nav.Link> 

		        	<Nav.Link className="m-3" as={Link} to="/cart"><i className="fa-solid fa-cart-shopping"></i></Nav.Link>
		        	</>
		        	:
		        	<>
		        	<Nav.Link className="m-3" as={Link} to="/signup">Signup</Nav.Link>

		        	<Nav.Link className="m-3" as={Link} to="/login">Login</Nav.Link>

		        	<Nav.Link className="m-3" as={Link} to="/login"><i className="fa-solid fa-cart-shopping"></i></Nav.Link>
		        	</>
		    	}

		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>
	)
}