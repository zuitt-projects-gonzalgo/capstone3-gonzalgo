import {useState, useEffect} from 'react';
import {Row, Col, Card, Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function ProductCard({productProp}) {
	
	const {artist, name, description, price, inventory, _id} = productProp

	return(
		<Card>
			<Card.Header as="h2">{artist}</Card.Header>
			<Card.Body>
				<Card.Title>{name}</Card.Title>
				<Card.Text>{description}</Card.Text>
				<Card.Text>&#8369; {price}</Card.Text>
				<Card.Subtitle>Inventory: <span>{inventory}</span></Card.Subtitle>
				<br/>
				<Button variant="primary" as={Link} to={`/products/${_id}`}>See Details</Button>
			</Card.Body>
		</Card>
	)
}