import {Fragment, useState, useEffect} from 'react';
import { Table, Button, Modal, Form } from 'react-bootstrap';
/*import {Link} from 'react-router-dom';*/
import Swal from 'sweetalert2';



export default function ProductAdmin(props) {

	const { fetchData } = props;

	const [productId, setProductId] = useState("");
	const [product, setProducts] = useState([]);
	const [artist, setArtists] = useState("");
	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [price, setPrice] = useState(0);
	const [inventory, setInventory] = useState(0)

	// States to open/close modals
		const [showEdit, setShowEdit] = useState(false);
		const [showAdd, setShowAdd] = useState(false);

		// Functions to toggle the opening and closing of the "Add Product" modal
			const openAdd = () => setShowAdd(true);
			const closeAdd = () => setShowAdd(false);

		const openEdit = (productId) => {

			// Fetches the selected course data using the product ID
			fetch(`https://evening-ravine-83251.herokuapp.com/products/${ productId }`)
			.then(res => res.json())
			.then(data => {


				// Changes the states for binded to the input fields
				// Populates the values of the input files in the modal form
				setProductId(data._id);
				setArtists(data.artist);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				setInventory(data.inventory);
			})

			// Opens the "Edit Course" modal
			setShowEdit(true);
		};

		const closeEdit = () => {

			setShowEdit(false);
			setArtists("");
			setName("");
			setDescription("");
			setPrice(0);
			setInventory(0);
		};

		const addProduct = (e) => {

			// Prevent the form from redirecting to a different page on submit 
			// Helps retain the data if adding a course is unsuccessful
			e.preventDefault()

			fetch(`https://evening-ravine-83251.herokuapp.com/products`, {
				method: 'POST',
				headers: {
					Authorization: `Bearer ${ localStorage.getItem('token') }`,
					'Content-Type': 'application/json'
				},
				body: JSON.stringify({
					artist: artist,
					name: name,
					description: description,
					price: price,
					inventory: inventory
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);

				// If the new course is successfully added
				if (data === true) {

					/*fetchData();*/

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully added."					
					})

					setArtists("")
					setName("")
					setDescription("")
					setPrice(0)
					setInventory(0)

					// Close the modal
					closeAdd();

				} else {

					/*fetchData();*/

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					})
				}
			})
		}

		const editProduct = (e, productId) => {
				
			e.preventDefault();

			fetch(`https://evening-ravine-83251.herokuapp.com/products/${ productId }`, {
				method: 'PUT',
				headers: {
					'Content-Type': 'application/json',
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					artist: artist,
					name: name,
					description: description,
					price: price,
					inventory: inventory
				})
			})
			.then(res => res.json())
			.then(data => {
				

				if (data === true) {

					fetchData();

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully updated."
					});

					closeEdit();

				} else {

					fetchData();

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}

			})
		}

		const archiveToggle = (productId, isActive) => {

			fetch(`https://evening-ravine-83251.herokuapp.com/products/${ productId }/archive`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {
				console.log(data)

				if (data === true) {
					

					

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully archived."
					});

				} else {

					

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		}

		const unarchiveToggle = (productId, isActive) => {

			fetch(`https://evening-ravine-83251.herokuapp.com/products/${ productId }/activate`, {
				method: 'PUT',
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${ localStorage.getItem('token') }`
				},
				body: JSON.stringify({
					isActive: !isActive
				})
			})
			.then(res => res.json())
			.then(data => {

				if (data === true) {
					

					

					Swal.fire({
						title: "Success",
						icon: "success",
						text: "Product successfully activated."
					});

				} else {

					

					Swal.fire({
						title: "Something went wrong",
						icon: "error",
						text: "Please try again."
					});

				}
			})
		};

	useEffect(() => {

		let token = localStorage.getItem('token')

			fetch('https://evening-ravine-83251.herokuapp.com/products/all',{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(productData => {
				console.log(productData)
			setProducts(productData.map(product => {

			return(

				<tr key={product._id}>
					<td>{product.artist}</td>
					<td>{product.name}</td>
					<td>{product.description}</td>
					<td>{product.price}</td>
					<td>{product.inventory}</td>
					<td>
						{product.isActive
							? <span>Available</span>
							: <span>Unavailable</span>
						}
					</td>
					<td>
						<Button
							variant="primary"
							size="sm"
							onClick={() => openEdit(product._id)}
						>
							Update
						</Button>
				
						{product.isActive
							?
							<Button 
								variant="danger" 
								size="sm" 
								onClick={() => archiveToggle(product._id, product.isActive)}
							>
								Disable
							</Button>
							:
							<Button 
								variant="success"
								size="sm"
								onClick={() => unarchiveToggle(product._id, product.isActive)}
							>
								Enable
							</Button>
						}
					</td>
				</tr>
				);
		}));

	})
	}, []);


	
	return(
		<Fragment>

			<div className="text-center my-4">
				<h2>ADMIN DASHBOARD</h2>
				<div className="d-flex justify-content-center">
					<Button variant="primary" onClick={openAdd}>Add New Product</Button>			
				</div>			
			</div>

			<Table striped bordered hover responsive>
				<thead className="bg-success text-white">
					<tr>
						<th>Artist</th>
						<th>Name</th>
						<th>Description</th>
						<th>Price</th>
						<th>Inventory</th>
						<th>Availability</th>
						<th>Actions</th>
					</tr>					
				</thead>
				<tbody>
					{product}
				</tbody>
			</Table>

			{/*ADD MODAL*/}
			<Modal show={showAdd} onHide={closeAdd}>
				<Form onSubmit={e => addProduct(e)}>
					<Modal.Header closeButton className="bg-success text-light">
						<Modal.Title>Add Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productArtist">
							<Form.Label>Artist</Form.Label>
							<Form.Control type="text" value={artist} onChange={e => setArtists(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productInventory">
							<Form.Label>Inventory</Form.Label>
							<Form.Control type="number" value={inventory}  onChange={e => setInventory(e.target.value)} required/>
						</Form.Group>
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeAdd}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>

			{/*EDIT MODAL*/}
			<Modal show={showEdit} onHide={closeEdit}>
				<Form onSubmit={e => editProduct(e, productId)}>
					<Modal.Header closeButton>
						<Modal.Title>Edit Product</Modal.Title>
					</Modal.Header>
					<Modal.Body>	
						<Form.Group controlId="productArtist">
							<Form.Label>Artist</Form.Label>
							<Form.Control type="text" value={artist} onChange={e => setArtists(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productName">
							<Form.Label>Name</Form.Label>
							<Form.Control type="text" value={name} onChange={e => setName(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productDescription">
							<Form.Label>Description</Form.Label>
							<Form.Control type="text" value={description}  onChange={e => setDescription(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productPrice">
							<Form.Label>Price</Form.Label>
							<Form.Control type="number" value={price}  onChange={e => setPrice(e.target.value)} required/>
						</Form.Group>
						<Form.Group controlId="productInventory">
							<Form.Label>Inventory</Form.Label>
							<Form.Control type="number" value={inventory}  onChange={e => setInventory(e.target.value)} required/>
						</Form.Group>						
					</Modal.Body>
					<Modal.Footer>
						<Button variant="secondary" onClick={closeEdit}>Close</Button>
						<Button variant="success" type="submit">Submit</Button>
					</Modal.Footer>
				</Form>
			</Modal>
			
		</Fragment>

	)
}