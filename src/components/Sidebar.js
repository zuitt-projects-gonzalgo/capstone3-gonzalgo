import {Row, Col, Nav} from 'react-bootstrap';

export default function Sidebar() {
	return(

		<div id="left-tab" defaultactivekey="first">
			<h4>Group</h4>
			
				<Col>
					<Nav variant="pills" className="flex-column">
						<Nav.Item>
							<Nav.Link eventKey="first">ALL PRODUCTS</Nav.Link>
						</Nav.Item>

						<Nav.Item>
							<Nav.Link eventKey="second">BLACK PINK</Nav.Link>
						</Nav.Item>

						<Nav.Item>
							<Nav.Link eventKey="third">ITZY</Nav.Link>
						</Nav.Item>

						<Nav.Item>
							<Nav.Link eventKey="fourth">TWICE</Nav.Link>
						</Nav.Item>
					</Nav>					
				</Col>
			
		</div>
	)
}