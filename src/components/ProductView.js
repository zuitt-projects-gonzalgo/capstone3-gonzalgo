import {useState, useEffect, useContext} from 'react';
import {Card, Button} from 'react-bootstrap';
import {useParams, Link} from 'react-router-dom';
/*import Swal from 'sweetalert2';*/

import UserContext from '../UserContext';

export default function ProductView() {
	
	const {user} = useContext(UserContext);
	
	/*const history = useNavigate();*/

	const [artist, setArtists] = useState('');
	const [name, setName] = useState('');
	const [description, setDescription] = useState('');
	const [price, setPrice] = useState(0);
	const [inventory, setInventory] = useState(0);

	const {productId} = useParams();

	/*const addToCart = (productId) => {
		fetch('https://evening-ravine-83251.herokuapp.com/orders', {
			method: "POST",
			headers: {
				"Content-Type" : "application/json",
				Authorization: `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				userId: userId,
				productId: productId,
				productName: name,
				price: price,
				quantity: quantity
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(data === true) {
				Swal.fire({
					title: "Add to cart Succesfully",
					icon: "success",
					text: "You have successfully enrolled in this course"
				})

				history("/products");

			} else {
				Swal.fire({
					title: "Something went wrong",
					icon: "error",
					text: "Please contact admin"
				})
			}
		})
	}*/

	useEffect(() => {
		console.log(productId);

		fetch(`https://evening-ravine-83251.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			

			setArtists(data.artist);
			setName(data.name);
			setDescription(data.description);
			setPrice(data.price);
			setInventory(data.inventory);

		})
	}, [productId])

	return(
		<Card className="text-center">
			<Card.Header>{artist}</Card.Header>
			<Card.Body>
				<Card.Title as="h1">{name}</Card.Title>
				<Card.Text>{description}</Card.Text>
				<Card.Text as="h3" className="text-warning">&#8369; {price}</Card.Text>

				<Card.Subtitle>Inventory: <span>{inventory}</span></Card.Subtitle>
				<br/>

				<label>Quantity:</label>
				<br/>
				<input type="number" min="1"/>
				<br/>
				<br/>

				{ user.id !== null ?
					
					<Button variant="success" /*onClick={() => order(productId)}*/>Add to Cart</Button>
					
					:
					<Link className="btn btn-danger" to="/login">Log in to order</Link>
				}
			</Card.Body>
		</Card>
	)
}