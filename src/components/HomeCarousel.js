import {Carousel} from 'react-bootstrap';

import '../App.css';

export default function() {
	return(
		<Carousel className="homeCarousel">
		  <Carousel.Item>
		    <img
		      className="w-100 hc"
		      src="https://images.pexels.com/photos/167092/pexels-photo-167092.jpeg"
		      alt="First slide"
		    />
		    <Carousel.Caption>
		      <h3>First slide label</h3>
		      <p>Nulla vitae elit libero, a pharetra augue mollis interdum.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="w-100"
		      src="https://images.pexels.com/photos/6875115/pexels-photo-6875115.jpeg"
		      alt="Second slide"
		    />

		    <Carousel.Caption>
		      <h3>Second slide label</h3>
		      <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		  <Carousel.Item>
		    <img
		      className="w-100"
		      src="https://images.pexels.com/photos/2988232/pexels-photo-2988232.jpeg?auto=compress&cs=tinysrgb&w=1260&h=750&dpr=1"
		      alt="Third slide"
		    />

		    <Carousel.Caption>
		      <h3>Third slide label</h3>
		      <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur.</p>
		    </Carousel.Caption>
		  </Carousel.Item>
		</Carousel>
	)
}