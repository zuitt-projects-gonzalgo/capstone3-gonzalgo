import {useState, useEffect} from 'react';
import {BrowserRouter as Router, Route, Routes} from 'react-router-dom';
import {useNavigate} from 'react-router-dom';

//imports from components
import AppNavbar from './components/AppNavbar';
import ProductView from './components/ProductView';

//import from pages
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Products from './pages/Products';
import Signup from './pages/Signup';
import Logout from './pages/Logout';
import Search from './pages/Search';

//import from external css
import './App.css';

import {UserProvider} from './UserContext';

function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
  });

  const unsetUser = () => {
    localStorage.clear();
  }

  useEffect(() => {
    fetch('https://evening-ravine-83251.herokuapp.com/users/details', {
      method: "POST",
      headers: {
        Authorization: `Bearer ${localStorage.getItem("token")}`
      }
    })
    .then(res => res.json())
    .then(data => {
      if(typeof data._id !== "undefined") {
        setUser({
          id: data._id,
          isAdmin: data.isAdmin
        })
      } else {
        setUser({
          id: null,
          isAdmin: null
        })
      }
    })
  }, [])


  return(
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
          <>
            <Routes>

              <Route exact path= "/"
                element={
                <>
                  <AppNavbar />
                  <Home /> 
                </>
                }/>              

              <Route exact path= "/signup"
                element={
                <>
                <AppNavbar/>
                <Signup/>
                </>
                }/>

              <Route exact path= "/login"
                element={
                <>
                <AppNavbar/>
                <Login/>
                </>
                }/>

              <Route exact path="/products"
                element={
                  <>
                  <AppNavbar/>
                  <Products/>
                  </>
                }
              />

              <Route exact path="/products/:productId"
                element={
                  <>
                  <AppNavbar/>
                  <ProductView/>
                  </>
                }
              />

              <Route exact path="/products/search"
                element={
                  <>
                  <AppNavbar/>
                  <Search/>
                  </>
                }
              />

              <Route exact path="/logout"
                element={
                  <>
                  <AppNavbar/>
                  <Logout/>
                  </>
                }
              />

              <Route exact path= "*" element={<Error/>}/>

            </Routes>
          </>
        </Router>
        </UserProvider>
  )
}

export default App;
