import {useState, useEffect, useContext} from	'react';
import {Navigate, useNavigate, Link} from 'react-router-dom';
import {Form, Button, Row, Col} from 'react-bootstrap';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Signup() {
	
	const {user} = useContext(UserContext);

	const history = useNavigate();

	const [username, setUsername] = useState('');
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function signupUser(e) {
		e.preventDefault();

		fetch('https://evening-ravine-83251.herokuapp.com/users/checkEmail', {
			method: 'POST',
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data)

			if(data === true) {
				Swal.fire({
					title: "Email Already Exists",
					icon: "error",
					text: "Please provide another email"
				})
			} else {
				fetch('https://evening-ravine-83251.herokuapp.com/users/register', {
					method: 'POST',
					headers: {
						'Content-Type' : 'application/json'
					},
					body: JSON.stringify({
						username: username,
						email: email,
						password: password
					})
				})
				.then(res => res.json())
				.then(data => {
					console.log(data)
					if(data === true) {
						setUsername('');
						setEmail('');
						setPassword('');

						Swal.fire({
							title: "Registration successful",
							icon: "success",
							text: "Welcome to JDG Shop"
						})

						history("/login")
					} else {
						Swal.fire({
							title: "Something went wrong",
							icon: "error",
							text: "Please try again"
						})
					}
				})
			}
			
		})
		setUsername('');
		setEmail('');
		setPassword('');
	};

	useEffect(() => {
		if(email !== '' && password !== '' && username !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [username, email, password])


	return(
		(user.id !== null) ?
		<Navigate to="/"/> :

		<Row className="backGround bg-success">
			<Col className="p-5">
				<h1 className="text-light pt-5 px-5 text-center">JDG Shop</h1>
				<h3 className="text-light text-center">Order Now!</h3>
			</Col>

			<Col>
				<Form onSubmit={e => signupUser(e)} className="bg-white p-5 m-5">
					<h3>Sign Up</h3>

					<Form.Group className="my-4" controlId="signupUsername">
						<Form.Control 
							type="text" 
							placeholder="Username"
							value={username}
							onChange={e => setUsername(e.target.value)}
							required
						/>
					</Form.Group>

					<Form.Group className="my-4" controlId="signupEmail">
						<Form.Control 
							type="email" 
							placeholder="Email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

				  	<Form.Group className="mb-4" controlId="signupPassword">
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password"
					    	value={password}
					    	onChange={e => setPassword(e.target.value)}
					    	required
					    />
				  	</Form.Group>

				 	<div className="d-grid gap-2 my-2">

					{ isActive ?
						<Button variant="success" type="submit" id="submitBtn">Signup</Button> :
						<Button variant="danger" type="submit" id="submitBtn" disabled>Signup</Button>
					}

					</div>
					<span>Already have an account? </span>
					<Link to="/login">Login</Link>
				</Form>
			</Col>
		</Row>
	)
}