import {Button} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import '../App'

export default function Error() {
	return(
		<div className="bgimg">
			<div className="errormsg">
				<h1 className=" error text-light">404: Page Not Found</h1>
				<p className=" error text-light">Go back to <Button as={Link} to="/" className="error" variant="danger">Home Page</Button> </p>
			</div>
		</div>
	)
}