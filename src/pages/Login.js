import {useState, useEffect, useContext} from 'react';
import {Navigate} from 'react-router-dom';
import {Form, Button, Row, Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import Swal from 'sweetalert2';

import UserContext from '../UserContext';

export default function Login(props) {

	const {user, setUser} = useContext(UserContext);

	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');
	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {
		e.preventDefault();

		fetch('https://evening-ravine-83251.herokuapp.com/users/login', {
			method: "POST",
			headers: {
				'Content-Type' : 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			console.log(data);

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: 'Login Successful',
					icon: 'success',
					text: 'Welcome to JDG Shop'
				})
			} else {
				Swal.fire({
					title: 'Authentication Failed',
					icon: 'error',
					text: 'Please check your credentials'
				})
			}
		})


		const retrieveUserDetails = (token) => {
			fetch('https://evening-ravine-83251.herokuapp.com/users/details', {
				method: "POST",
				headers: {
					Authorization: `Bearer ${token}`
				}
			})
			.then(res => res.json())
			.then(data => {
				console.log(data);
				setUser({
					id: data._id,
					isAdmin: data.isAdmin
				})
			})
		}

		setEmail('');
		setPassword('');
		
	}

	useEffect(() => {
		if(email !== '' && password !== '') {
			setIsActive(true);
		} else {
			setIsActive(false);
		}
	}, [email, password]);

	return(
		(user.id !== null) ?
		<Navigate to="/"/> :

		<Row className="backGround bg-success">
			<Col className="p-5">
				<h1 className="text-light pt-5 px-5 text-center">JDG Shop</h1>
				<h3 className="text-light text-center">Order Now!</h3>
			</Col>

			<Col>
				<Form onSubmit={e => authenticate(e)} className="bg-white p-5 m-5">
					<h3>Log In</h3>
					<Form.Group className="my-4" controlId="loginEmail">
						<Form.Control 
							type="email" 
							placeholder="Email"
							value={email}
							onChange={e => setEmail(e.target.value)}
							required
						/>
					</Form.Group>

				  	<Form.Group className="mb-4" controlId="loginPassword">
					    <Form.Control 
					    	type="password" 
					    	placeholder="Password"
					    	value={password}
					    	onChange={e => setPassword(e.target.value)}
					    	required
					    />
				  	</Form.Group>
				 	<div className="d-grid gap-2 my-2">

					{ isActive ?
						<Button variant="success" type="submit" id="submitLoginBtn">Login</Button> :
						<Button variant="danger" type="submit" id="submitLoginBtn" disabled>Login</Button>
					}

					</div>
					<span>Does not have an account? </span>
					<Link to="/signup">Sign up</Link>
				</Form>
			</Col>
		</Row>
	)
}