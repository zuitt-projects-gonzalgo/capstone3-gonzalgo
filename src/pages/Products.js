import {useState, useEffect, useContext} from 'react';
import {Row, Col, Card, Button, Container} from 'react-bootstrap';

import Sidebar from '../components/Sidebar';
import ProductCard from '../components/ProductCard';
import ProductAdmin from '../components/ProductAdmin';
import ProductUser from '../components/ProductUser';

import UserContext from '../UserContext';

export default function Products() {

	const [products, setProducts] = useState([])

	const {user} = useContext(UserContext);

	const fetchData = () => {
			let token = localStorage.getItem('token')

			fetch('https://evening-ravine-83251.herokuapp.com/products/all',{
				method: "GET",
				headers: {
					"Authorization": `Bearer ${token}`
				}
			})
			.then(result => result.json())
			.then(data => {
				console.log(data)
				setProducts(data)
			})
		}

	useEffect(() => {
		fetchData()
	}, [])


	return(
		<>
		<Container>
			<Row className="my-4">
				{(user.isAdmin === true) ?
					<Col>
					<ProductAdmin productData={products} fetchData={fetchData}/> 
					</Col>
					:
					<>
					<Col md={3}>
						<Sidebar/>
					</Col>
					<Col>
						<ProductUser productData={products}/>
					</Col>
					</>}
			</Row>
			</Container>
		</>
	)
}