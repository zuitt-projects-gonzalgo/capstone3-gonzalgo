import {useContext, useEffect} from 'react';
import {Navigate} from 'react-router-dom';

import UserContext from '../UserContext';

export default function Logout() {

	const {unsetUser, setUser} = useContext(UserContext);

	//clears the localStorage of the user's information
	unsetUser();
	/*localStorage.clear();*/

	useEffect(() => {
		setUser({id: null})
	}, [setUser])

	return(
		<Navigate to='/login'/>	
	);
}